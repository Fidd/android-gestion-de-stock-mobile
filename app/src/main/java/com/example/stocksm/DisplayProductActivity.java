package com.example.stocksm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.stocksm.util.Util;

public class DisplayProductActivity extends AppCompatActivity {

    private ImageView dp_img;
    private TextView dp_name, dp_price, dp_pAchat, dp_qty, dt_desc, dp_category, dp_date;
    private Button dp_edit, dp_remove, dp_action, dp_inventaire_link;

    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_product);
        link();
        id = getIntent().getIntExtra("product",-1);

        dp_img.setImageResource(Util.getMipmapResIdByName(DisplayProductActivity.this,Util.getProductList().get(id).getImgPath()));
        dp_name.setText(Util.getProductList().get(id).getName());
        double dpP=Util.getProductList().get(id).getPrice()+100;
        dp_price.setText(""+dpP+" f");
        dp_pAchat.setText(""+Util.getProductList().get(id).getPrice()+ " f");
        dp_qty.setText(""+Util.getProductList().get(id).getQty());
        dt_desc.setText(Util.getProductList().get(id).getDesc());
        dp_category.setText(Util.getProductList().get(id).getCategory().getName());
        //dp_date.setText(Util.getProductList().get(id).getDateCreate().toString());

        dp_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayProductActivity.this, ProductEditActivity.class);
                intent.putExtra("product", id);
                startActivity(intent);
            }
        });
        dp_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayProductActivity.this, ActionProductActivity.class);
                intent.putExtra("product", id);
                startActivity(intent);
            }
        });
        dp_inventaire_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayProductActivity.this, Inventaire.class);
                intent.putExtra("product", id);
                startActivity(intent);
            }
        });

        dp_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DisplayProductActivity.this,"Product deleted",Toast.LENGTH_SHORT).show();
            }
        });


    }

    void link(){
        dp_img = (ImageView) findViewById(R.id.dp_img);
        dp_name = (TextView)findViewById(R.id.dp_name);
        dp_price = (TextView)findViewById(R.id.dp_price);
        dp_pAchat = (TextView)findViewById(R.id.dp_pAchat);
        dp_qty = (TextView)findViewById(R.id.dp_qty);
        dt_desc = (TextView)findViewById(R.id.dp_desc);
        dp_category = (TextView)findViewById(R.id.dp_category);
        dp_date = (TextView)findViewById(R.id.dp_date);

        dp_edit = (Button)findViewById(R.id.dp_edit);
        dp_remove = (Button)findViewById(R.id.dp_remove);
        dp_action = (Button)findViewById(R.id.dp_action);
        dp_inventaire_link = (Button)findViewById(R.id.dp_inventaire_link);
    }
}