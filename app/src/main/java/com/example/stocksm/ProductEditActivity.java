package com.example.stocksm;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.stocksm.model.Category;
import com.example.stocksm.model.Product;
import com.example.stocksm.util.Util;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ProductEditActivity extends AppCompatActivity {
    List<Category> listCategory;
    private Product product;

    private EditText ep_name, ep_desc, ep_price, ep_qty;
    private ImageView ep_img;
    private Button ep_submit;
    private Spinner ep_cat;
    private Category category;
    private ArrayAdapter<Category> arrayAdapterOfCategory;

    int id=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);

        id = getIntent().getIntExtra("product", -1);
        link();
        init();

        arrayAdapterOfCategory = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listCategory);
        arrayAdapterOfCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ep_cat.setAdapter(arrayAdapterOfCategory);

        ep_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
                Toast.makeText(ProductEditActivity.this, "Vous devez choisir une image", Toast.LENGTH_LONG).show();
            }
        });

        ep_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = (Category)ep_cat.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                category=null;
            }
        });

        ep_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUpdateSubmit();
                finish();
            }
        });

    }


    private  void link(){
        ep_name = (EditText) findViewById(R.id.ep_name);
        ep_desc =  (EditText) findViewById(R.id.ep_desc);
        ep_price =  (EditText) findViewById(R.id.ep_price);
        ep_qty = (EditText) findViewById(R.id.ep_quantity);
        ep_img = (ImageView) findViewById(R.id.ep_img);
        ep_submit=(Button)findViewById(R.id.ep_submit);
        ep_cat = (Spinner)findViewById(R.id.ep_cat);

        listCategory = Util.getCategoryList();
    }
    private void init(){
        if(id>-1){
            product = Util.getProductList().get(id);
            ep_name.setHint(product.getName());
            ep_desc.setHint(product.getDesc());
            ep_price.setHint(product.getPrice()+"");
            ep_qty.setHint(product.getQty()+"");
            ep_img.setImageResource(Util.getMipmapResIdByName(ProductEditActivity.this, product.getImgPath()));
            //ep_cat.setSelection(listCategory.indexOf(product.getCategory()));
        }


    }
    private void  onUpdateSubmit(){
        String name= ep_name.getText().toString().trim(), desc = ep_desc.getText().toString().trim();

        Double price = -1.0;
        int qty = -1;
        try{
            price = Double.parseDouble(ep_price.getText().toString());
            qty = Integer.parseInt(ep_qty.getText().toString());
        }catch (NumberFormatException e){
        }


        boolean productIsModify = false;

        if(!name.isEmpty()){
            product.setName(name);
            productIsModify=true;
        }

        if (!desc.isEmpty()) {
            product.setDesc(desc);
            productIsModify = true;
        }
        if(price > 0.0) {
            product.setPrice(price);
            productIsModify = true;
        }
        if(qty>0) {
            product.setQty(qty);
            productIsModify = true;
        }
        if(category !=null){
            product.setCategory(category);
            productIsModify = true;
        }

        if(productIsModify)
            Util.getProductList().set(id, product);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){

            try {
                Uri imageUri = data.getData();
                InputStream imageStram = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStram);
                ep_img.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Erreur de traitement de l'image", Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(this, "Vous n'avez selectioner aucune image", Toast.LENGTH_SHORT).show();
        }
    }
}