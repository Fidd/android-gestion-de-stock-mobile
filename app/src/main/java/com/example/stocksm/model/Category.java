package com.example.stocksm.model;

public class Category {
    private String name, code;
    private int id;

    public Category(String name, String code, int id) {
        this.name = name;
        this.code = code;
        this.id = id;
    }

    public Category() {
    }

    public Category(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }
}
