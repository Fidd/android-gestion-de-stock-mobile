package com.example.stocksm.model;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.stocksm.R;
import com.example.stocksm.util.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductAdapter  extends ArrayAdapter<Product> implements Filterable {
    ArrayList<Product> origineProductList;

    private ArrayList<Product> listProduct;
    private Context context;
    private LayoutInflater layoutInflater;

    public ProductAdapter(Context context, ArrayList<Product> listProduct) {
        super(context,0);
        origineProductList=listProduct;
        this.listProduct = listProduct;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listProduct.size();
    }

    @Override
    public Product getItem(int position) {
        return listProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.product_item,null);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.nameTv);
            viewHolder.desc = (TextView) convertView.findViewById(R.id.descTv);
            viewHolder.price = (TextView) convertView.findViewById(R.id.priceTv);
            viewHolder.qty = (TextView) convertView.findViewById(R.id.quantityTv);
            viewHolder.imgPath = (ImageView) convertView.findViewById(R.id.imgIv);

            convertView.setTag(viewHolder);
        }else{
            viewHolder= (ViewHolder)convertView.getTag();
        }
        Product product = this.listProduct.get(position);
        viewHolder.name.setText(product.getName());
        viewHolder.desc.setText(product.getDesc());
        viewHolder.price.setText("P: "+product.getPrice()+"");
        viewHolder.qty.setText("Q: "+product.getQty()+"");

        int imgid = Util.getMipmapResIdByName(context, product.getImgPath());

        viewHolder.imgPath.setImageResource(imgid);

        return convertView;
    }

    static class  ViewHolder{
        TextView name, desc, price, qty, id, dateCreate;
        ImageView  imgPath;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                ArrayList<Product> products = new ArrayList<>();
                if(origineProductList == null){
                    origineProductList = new ArrayList<Product>(listProduct);
                }
                if(constraint == null || constraint.length()==0){
                    filterResults.count = origineProductList.size();
                    filterResults.values = origineProductList;
                }else{
                    String data = constraint.toString().toUpperCase();
                    origineProductList.forEach(product -> {
                        if(product.startWith(data)){
                            products.add(new Product(product));
                        }
                    });
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        origineProductList.forEach(product -> {
                            if(product.notStartWith(data)){
                                products.add(new Product(product));
                            }
                        });
                    }
                    filterResults.count = products.size();
                    filterResults.values = products;
                }
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listProduct = (ArrayList<Product>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
