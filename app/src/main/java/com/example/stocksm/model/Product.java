package com.example.stocksm.model;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.Date;

public class Product {
    private String name, desc, imgPath;
    private double price;
    private int qty, id;
    private Date dateCreate;
    private Category category;


    public Product(String name, String desc, Category cat, String imgPath, double price, int qty) {
        this.name = name;
        this.desc = desc;
        this.imgPath = imgPath;
        this.price = price;
        this.qty = qty;
        this.category = cat;
        dateCreate = new Date();
    }

    public Product(String name, String desc, Category cat, String imgPath, double price, int qty, int id, Date dateCreate) {
        this(name,desc,cat, imgPath,price,qty);
        this.id = id;
        this.dateCreate = dateCreate;
    }
    public Product (Product product){
        this.name = product.name;
        desc = product.desc;
        category = product.category;
        imgPath = product.imgPath;
        price = product.price;
        qty = product.qty;
        dateCreate = product.dateCreate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    /*****OPERATORS******/
    public boolean sell( @NonNull int quantity){
        if(quantity>0 & qty > quantity){
            qty -= quantity;
            return true;
        }
        return false;
    }
    public boolean more(int qte){
        if(qte>0){
            qty += qte;
            return true;
        }
        return false;
    }
    public boolean startWith(@NonNull  String tosearch){
        return name.toUpperCase().startsWith(tosearch.toUpperCase()) || desc.toUpperCase().startsWith(tosearch.toUpperCase());
    }
    public boolean search(@NonNull  String tosearch){
        return name.toUpperCase().contains(tosearch.toUpperCase()) || desc.toUpperCase().contains(tosearch.toUpperCase());
    }
    public boolean notStartWith(@NonNull  String tosearch){
        return !startWith(tosearch) && search(tosearch);
    }

}
