package com.example.stocksm;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.stocksm.model.Product;
import com.example.stocksm.util.Util;

public class ActionProductActivity extends AppCompatActivity {
    private Button action_more, action_sell, action_cancel ;
    private EditText actionProduct_en;

    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_product);
        id = getIntent().getIntExtra("product", -1);

        action_cancel = (Button) findViewById(R.id.action_cancel);
        action_more = (Button) findViewById(R.id.action_more);
        action_sell = (Button) findViewById(R.id.action_sell);
        actionProduct_en = (EditText)findViewById(R.id.avtionProduct_en);

        action_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        action_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more();
            }
        });
        action_sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell();
            }
        });
    }

    void sell(){
        int nbre= Util.getInt(actionProduct_en, 0);
        if (nbre<1){
            Util.getDialog(this, "Qunatite invalide. Veillez verifier que c'est un entier superieur à 1 ", "Erreur").show();
        }else if(!Util.getProductList().get(id).sell(nbre)){
            Util.getDialog(this, "Quantite Insuffisant ", "Erreur").show();
        }else{
            finish();
        }
    }
    void more(){
        int nbre= Util.getInt(actionProduct_en, 0);
        if (nbre<1){
            Util.getDialog(this, "Quantite invalide. Veillez verifier que c'est un entier superieur à 1", "Erreur").show();
        }else{
            Util.getProductList().get(id).more(nbre);
            finish();
        }
    }
}