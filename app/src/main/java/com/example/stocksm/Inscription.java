package com.example.stocksm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Inscription extends AppCompatActivity {
    private EditText first_name;
    private   EditText last_name;
    private   EditText email;
    private   EditText password;
    private Button buttonSign;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        last_name = findViewById(R.id.last_name);
        first_name = findViewById(R.id.first_name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        buttonSign = findViewById(R.id.buttonSign);
        buttonSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String LName="", FName, password, email;
                LName=last_name.getText().toString();
                Intent intent= new Intent(Inscription.this , Login.class) ;
                startActivity(intent);
            }
        });
    }
}