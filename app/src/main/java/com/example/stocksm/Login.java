package com.example.stocksm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends AppCompatActivity {
    private EditText usernameLogin;
    private EditText passLogin;
    private Button loginButton;
    private Button signButtonLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameLogin=findViewById(R.id.email);
        passLogin=findViewById(R.id.password);
        loginButton=findViewById(R.id.loginButton);
        signButtonLogin=findViewById(R.id.signButtonLogin);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName ,passUser;
                userName= String.valueOf(usernameLogin.getText());
                passUser=String.valueOf(passLogin.getText());

//                Log.d("Verification","var   " + userName + "  vari  " + passUser);
                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);
            }
        });
        signButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Inscription.class);
                startActivity(intent);
            }
        });
    }
}