package com.example.stocksm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.stocksm.util.Util;

public class Inventaire extends AppCompatActivity {

    private TextView textView17,ip_qty_init,ip_price_init,ip_qty_import,ip_price_import,ip_price_total,ip_qty_total,ip_qty_sell,ip_price_sell,ip_qty_in_hand,ip_price_in_hand;
    private Button quitter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventaire);
        link();

        int id=getIntent().getIntExtra("product",-1);
        ip_qty_init.setText(""+Util.getProductList().get(id).getQty());
        ip_price_init.setText(""+Util.getProductList().get(id).getPrice());

        int Q=Util.getProductList().get(id).getQty();
        ip_qty_total.setText(""+Q);
        ip_price_total.setText(""+Util.getProductList().get(id).getPrice()*Q);

        textView17.setText("Benefice "+15000);

        quitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void link(){
        ip_qty_init =  findViewById(R.id.ip_qty_init);
        ip_price_init = findViewById(R.id.ip_price_init);

        ip_qty_import = (TextView)findViewById(R.id.ip_qty_import);
        ip_price_import = (TextView)findViewById(R.id.ip_price_import);

        ip_price_total = (TextView)findViewById(R.id.ip_price_total);
        ip_qty_total = (TextView)findViewById(R.id.ip_qty_total);

        ip_qty_sell = (TextView)findViewById(R.id.ip_qty_sell);
        ip_price_sell = (TextView)findViewById(R.id.ip_price_sell);

        ip_qty_in_hand = findViewById(R.id.ip_qty_in_hand);
        ip_price_in_hand = findViewById(R.id.ip_price_in_hand);

        textView17=findViewById(R.id.textView17);
        quitter=findViewById(R.id.button);
    }
}