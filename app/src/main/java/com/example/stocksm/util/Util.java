package com.example.stocksm.util;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.stocksm.model.Category;
import com.example.stocksm.model.Product;

import java.util.ArrayList;
import java.util.List;

public class Util {

    public static List<Product> productList = new ArrayList<>();
    public static List<Category> categoryList= new ArrayList<>();


    public static List<Product> getProductList(){
        if(productList.isEmpty())
            initProductList();
        return productList;
    }

    public static List<Category> getCategoryList(){
        if(categoryList.isEmpty())
            initCategoryList();
        return categoryList;
    }
    public static int getMipmapResIdByName(Context context, String resName){
        String pkgName = context.getPackageName();
        int resId = context.getResources().getIdentifier(resName,"drawable", pkgName);
        Log.i("CustomerView", "ressources"+resName+"==>Res ID= "+resId);
        return  resId;
    }

    public static void initProductList(){
        productList.add(new Product("Gnangan Pobole","Le meilleur Medicament pour vous traiter vos fatigue, GNANGAN POBOLE", categoryList.get(0),"img1", 100.0,50));
        productList.add(new Product("Wô félé","Operation rapide à 7h, testeer une fois et vous n'aller point jamain vider votre cuisine de pate le nuit ", categoryList.get(1),"img2",200.0,51));
        productList.add(new Product("Piscine","Venez prendre votre melange de gari, lait, arrachide plus eau", categoryList.get(7),"img1",100.0,50));
        productList.add(new Product("Harico Bobo","Avec seulement l'huile et du garie sec, vous regretterai de pas avoir connu cet haricot avant", categoryList.get(2),"img2",200.0,51));
        productList.add(new Product("Man tingen","Venez payer pour mnanger le meilleur legume. Svp, appreter vos doit pour suporter le cout de vos dant", categoryList.get(4),"img3",300.0,52));
        productList.add(new Product("Wô koli","Wô koli est un alimentation aliment qui donne  beaucoup de la force.", categoryList.get(3),"img4",400.0,53));
        productList.add(new Product("Gali Gnignan","Le Meileir mais d'afrique et du monde en generale", categoryList.get(4),"img5",500.0,54));
        productList.add(new Product("Agbehoungbe Mayoto","Connu sous le nom de Sodabi, Agbahoungue est le vin le plus populaire de l'affrique.", categoryList.get(5),"img6",700.0,55));
        productList.add(new Product("Kpaki","Connue sous le nom de manioc, Kpaki est un tubercule fortement ulitiser dans l'alimentation beninois.", categoryList.get(0),"img7",600.0,56));
        productList.add(new Product("Soja","Le Meileir mais d'afrique et du monde en generale", categoryList.get(2),"img3",300.0,52));
        productList.add(new Product("Haricot","Le Meileir mais d'afrique et du monde en generale", categoryList.get(3),"img4",400.0,53));
        productList.add(new Product("Mais","Le Meileir mais d'afrique et du monde en generale", categoryList.get(4),"img5",500.0,54));
        productList.add(new Product("Igname","Le Meileir mais d'afrique et du monde en generale", categoryList.get(5),"img6",700.0,55));
        productList.add(new Product("Table","Le Meileir mais d'afrique et du monde en generale", categoryList.get(6),"img7",600.0,56));

    }
    public  static  void initCategoryList(){
        categoryList.add(new Category("Categorie1","CAT1"));
        categoryList.add(new Category("Categorie2","CAT2"));
        categoryList.add(new Category("Categorie3","CAT3"));
        categoryList.add(new Category("Categorie4","CAT4"));
        categoryList.add(new Category("Categorie5","CAT1"));
        categoryList.add(new Category("Categorie6","CAT2"));
        categoryList.add(new Category("Categorie7","CAT3"));
        categoryList.add(new Category("Categorie8","CAT4"));
        categoryList.add(new Category("Categorie9","CAT4"));

    }

    public static  int getInt(EditText editText, int defaut){
        try {
            return Integer.parseInt(editText.getText().toString());
        }catch (NumberFormatException e){
            return defaut;
        }
    }
    public static  double getDouble(EditText editText, double defaut){
        try {
            return Double.parseDouble(editText.getText().toString());
        }catch (NumberFormatException e){
            return defaut;
        }
    }
    public static AlertDialog getDialog(Context context,String content, String title){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(content);
        AlertDialog dialog = builder.create();
        return  dialog;
    }
}
