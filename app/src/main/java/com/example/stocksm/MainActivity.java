package com.example.stocksm;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.stocksm.model.Category;
import com.example.stocksm.model.Product;
import com.example.stocksm.model.ProductAdapter;
import com.example.stocksm.util.Util;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    List<Category> listCategory;
    private AlertDialog alertDialog;
    private SearchView search_product;
    private  ProductAdapter productAdapter;

    FloatingActionButton addProduct_fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addProduct_fab = (FloatingActionButton) findViewById(R.id.addProduct_fab);
        search_product = (SearchView)findViewById(R.id.product_sv);

        listCategory= Util.getCategoryList();

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        //builder.setMessage("Operation reussisse").setTitle("Success");
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        builder.setView(layoutInflater.inflate(R.layout.activity_product_edit, null));

        builder.setNegativeButton(" Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
        alertDialog = builder.create();

        ArrayList<Product> listProduct = (ArrayList<Product>) Util.getProductList();


        listView = (ListView)findViewById(R.id.listView);
        productAdapter = new ProductAdapter(this, listProduct);
        listView.setAdapter(productAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ActionProductActivity.class);
                intent.putExtra("product", position);
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DisplayProductActivity.class);
                intent.putExtra("product", position);
                startActivity(intent);
                return true;
            }
        });

        addProduct_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CreateProductActivity.class);
                startActivity(intent);
            }
        });
        search_product.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                productAdapter.getFilter().filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                productAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }
}