package com.example.stocksm;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.stocksm.model.Category;
import com.example.stocksm.model.Product;
import com.example.stocksm.util.Util;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class CreateProductActivity extends AppCompatActivity {

    List<Category> listCategory;
    private Product product;

    private EditText cp_name, cp_desc, cp_price, cp_qty;
    private ImageView cp_img;
    private Button cp_submit;
    private Spinner cp_cat;
    private Category category;
    private ArrayAdapter<Category> arrayAdapterOfCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);


        link();

        arrayAdapterOfCategory = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listCategory);
        arrayAdapterOfCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cp_cat.setAdapter(arrayAdapterOfCategory);

        cp_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(CreateProductActivity.this, "Vous devez choisir une image", Toast.LENGTH_LONG).show();
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
                Toast.makeText(CreateProductActivity.this, "Vous devez choisir une image", Toast.LENGTH_LONG).show();
            }
        });

        cp_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = (Category)cp_cat.getSelectedItem();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                category=null;
            }
        });

        cp_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateSubmited();
                finish();
            }
        });
    }


    private  void link(){
        cp_name = (EditText) findViewById(R.id.cp_name);
        cp_desc =  (EditText) findViewById(R.id.cp_desc);
        cp_price =  (EditText) findViewById(R.id.cp_price);
        cp_qty = (EditText) findViewById(R.id.cp_quantity);
        cp_img = (ImageView) findViewById(R.id.cp_img);
        cp_submit=(Button)findViewById(R.id.cp_submit);
        cp_cat = (Spinner)findViewById(R.id.cp_cat);

        listCategory = Util.getCategoryList();
    }


    private void onCreateSubmited(){
        String name= cp_name.getText().toString().trim(), desc = cp_desc.getText().toString().trim();
        Double price = Util.getDouble(cp_price, -1);
        int qty = Util.getInt(cp_qty, -1);
        if(!name.isEmpty() && !desc.isEmpty() && price > 0 && qty>0 && category != null) {
            product = new Product(name, desc, category, "", price, qty);
            Util.getProductList().add(product);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){

            try {
                Uri imageUri = data.getData();
                InputStream imageStram = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStram);
                cp_img.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Erreur de traitement de l'image", Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(this, "Vous n'avez selectioner aucune image", Toast.LENGTH_SHORT).show();
        }
    }
}